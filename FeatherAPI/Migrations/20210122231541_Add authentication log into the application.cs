﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FeatherAPI.Migrations
{
    public partial class Addauthenticationlogintotheapplication : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AuthenticationLogs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    IpAddress = table.Column<string>(nullable: true),
                    Agent = table.Column<string>(nullable: true),
                    AuthenticationTime = table.Column<DateTime>(nullable: false),
                    UserCredentialRef = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthenticationLogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AuthenticationLogs_AspNetUsers_UserCredentialRef",
                        column: x => x.UserCredentialRef,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AuthenticationLogs_UserCredentialRef",
                table: "AuthenticationLogs",
                column: "UserCredentialRef");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AuthenticationLogs");
        }
    }
}
