﻿namespace FeatherAPI.DTO
{
    public class DepartmentReport
    {
        public int All { get; set; }
        public int Manager { get; set; }
        public int Employee { get; set; }
    }
}