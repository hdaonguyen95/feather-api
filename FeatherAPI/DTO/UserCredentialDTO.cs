﻿using System.ComponentModel.DataAnnotations;

namespace FeatherAPI.DTO
{
    public class UserCredentialDTO
    {
        public string Id { get; set; }
        public string Email { get; set; }

        [Required(ErrorMessage = "Username is required")]
        public string UserName { get; set; }

        public string Password { get; set; }
        public string ConfirmedPassword { get; set; }
        public string PhoneNumber { get; set; }
    }
}