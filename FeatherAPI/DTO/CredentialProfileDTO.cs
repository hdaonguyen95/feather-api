﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FeatherAPI.DTO
{
    public class CredentialProfileDTO
    {
        public string Username { get; set; }
        public int Id { get; set; }
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "First name is required")]
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        [Required(ErrorMessage = "Last name is required")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Social security number is required")]
        public string SocialSecurityNumber { get; set; }

        [Required(ErrorMessage = "Address street is required")]
        public string AddressStreet { get; set; }

        [Required(ErrorMessage = "Address city is required")]
        public string AddressCity { get; set; }

        [Required(ErrorMessage = "Address state is required")]
        public string AddressState { get; set; }

        [Required(ErrorMessage = "Address postal code is required")]
        public string AddressPostalCode { get; set; }

        public bool IsManager { get; set; }
        public string EmploymentStatus { get; set; }
        public DateTime HiredDate { get; set; }
        public int? ManagerIdentityNumber { get; set; }
        public int? DepartmentForeignKey { get; set; }
        public string CredentialForeignKey { get; set; }
    }
}