using System;

namespace FeatherAPI.DTO
{
    public class AuthenticationLogDTO
    {
        public int Id { get; set; }
        public string IpAddress { get; set; }
        public string Agent { get; set; }
        public DateTime AuthenticationTime { get; set; }
    }
}