﻿using System.ComponentModel.DataAnnotations;

namespace FeatherAPI.DTO
{
    public class DepartmentDTO
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Department name is required")]
        public string DepartmentName { get; set; }
    }
}