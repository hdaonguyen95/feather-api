﻿namespace FeatherAPI.DTO
{
    public class PhotoDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public byte[] PhotoBinary { get; set; }
    }
}