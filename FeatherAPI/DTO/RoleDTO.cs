﻿using System.ComponentModel.DataAnnotations;

namespace FeatherAPI.DTO
{
    public class RoleDTO
    {
        public string UserName { get; set; }
        public string RoleName { get; set; }
    }
}