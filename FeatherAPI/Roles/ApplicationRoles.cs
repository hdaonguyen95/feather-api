﻿namespace FeatherAPI.Roles
{
    public class ApplicationRoles
    {
        public const string CanModifyCredentialDatabase = "CanModifyCredentialDatabase";
        public const string CanModifyDepartmentDatabase = "CanModifyDepartmentDatabase";
    }
}