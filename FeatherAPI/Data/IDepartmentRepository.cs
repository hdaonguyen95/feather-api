﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FeatherAPI.DTO;
using FeatherAPI.Models;

namespace FeatherAPI.Data
{
    public interface IDepartmentRepository
    {
        Task<Department> CreateDepartment(string departmentName);
        Task<Department> RetrieveDepartmentByNumber(int departmentNumber);
        Task<IEnumerable<Department>> RetrieveAllDepartments();
        Task<Department> UpdateDepartment(int departmentNumber, Department updates);
        Task<bool> RemoveDepartment(int departmentNumber);
        Task<bool> IsDepartmentExists(Department department);
        Task<Department> GetDepartmentById(int id);

        Task<DepartmentReport> GenerateDepartmentReport(int id);
    }
}