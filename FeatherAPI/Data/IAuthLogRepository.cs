using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FeatherAPI.DTO;
using FeatherAPI.Models;

namespace FeatherAPI.Data
{
    public interface IAuthLogRepository
    {
        Task<AuthenticationLog> CreateLog(string username, string ipAddress, string agent);
        Task<IEnumerable<AuthenticationLogDTO>> RetrieveLog(string username);
    }
}