﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FeatherAPI.Models;
using Microsoft.AspNetCore.Identity;

namespace FeatherAPI.Data
{
    public class RoleRepository : IRoleRepository
    {
        private readonly UserManager<UserCredential> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public RoleRepository(UserManager<UserCredential> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public async Task<bool> AddCredentialToRole(UserCredential credential, string roleName)
        {
            try
            {
                var record = await _userManager.FindByNameAsync(credential.UserName);
                if (record == null)
                    return false;
                var result = await _userManager.AddToRoleAsync(record, roleName);
                if (result.Succeeded)
                    return true;
                throw new Exception(result.Errors.ToString());
            }
            catch (Exception exception)
            {
                await Console.Error.WriteLineAsync(exception.Message);
                return false;
            }
        }

        public async Task<bool> RemoveCredentialFromRole(UserCredential credential, string roleName)
        {
            try
            {
                var record = await _userManager.FindByNameAsync(credential.UserName);
                if (record == null)
                    return false;
                var result = await _userManager.RemoveFromRoleAsync(record, roleName);
                if (result.Succeeded)
                    return true;
                throw new Exception(result.Errors.ToString());
            }
            catch (Exception exception)
            {
                await Console.Error.WriteLineAsync(exception.Message);
                return false;
            }
        }

        public async Task<bool> CreateRole(string roleName)
        {
            try
            {
                if (await _roleManager.RoleExistsAsync(roleName))
                    return false;
                // ReSharper disable once HeapView.ObjectAllocation.Evident
                var result = await _roleManager.CreateAsync(new IdentityRole(roleName));
                if (result.Succeeded)
                    return true;
                throw new Exception(result.Errors.ToString());
            }
            catch (Exception exception)
            {
                await Console.Error.WriteLineAsync(exception.Message);
                return false;
            }
        }
    }
}