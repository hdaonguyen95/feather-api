﻿using System;
using System.Threading.Tasks;
using FeatherAPI.DTO;
using FeatherAPI.Models;

namespace FeatherAPI.Data
{
    public interface ICredentialRepository
    {
        Task<UserCredentialDTO> CreateCredential(UserCredentialDTO userCredentialDto);
        Task<bool> SignIn(UserCredentialDTO userCredential);
        Task SignOut();
        Task<bool> UpdateCredential(string username, UserCredentialDTO updates);
        Task<bool> ChangePassword(string username, string password);
        Task<bool> RemoveCredential(UserCredentialDTO userCredential);
        Task<bool> IsCredentialExists(UserCredentialDTO userCredential);
        Task<string> GenerateToken(UserCredentialDTO userCredential);
    }
}