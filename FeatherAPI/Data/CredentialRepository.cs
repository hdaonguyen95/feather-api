﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using FeatherAPI.DTO;
using FeatherAPI.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace FeatherAPI.Data
{
    public class CredentialRepository : ICredentialRepository
    {
        private readonly UserManager<UserCredential> _userManager;
        private readonly SignInManager<UserCredential> _signInManager;
        private readonly IConfiguration _configuration;
        private readonly IMapper _autoMapper;

        public CredentialRepository(UserManager<UserCredential> userManager,
            SignInManager<UserCredential> signInManager, IConfiguration configuration, IMapper autoMapper)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _autoMapper = autoMapper;
        }

        public async Task<UserCredentialDTO> CreateCredential(UserCredentialDTO userCredential)
        {
            if (await IsCredentialExists(userCredential))
                return null;

            var result = await _userManager
                // ReSharper disable once HeapView.ObjectAllocation.Evident
                .CreateAsync(new UserCredential
                {
                    UserName = userCredential.UserName,
                    Email = userCredential.Email
                }, userCredential.Password);

            return result.Succeeded ? userCredential : null;
        }

        public async Task<bool> SignIn(UserCredentialDTO userCredential)
        {
            var record = await _userManager.FindByNameAsync(userCredential.UserName);
            if (record == null)
                return false;

            var result = await _signInManager
                .PasswordSignInAsync(userCredential.UserName,
                    userCredential.Password,
                    false, true);
            return result.Succeeded;
        }

        public async Task SignOut()
        {
            await _signInManager.SignOutAsync();
        }

        public async Task<bool> UpdateCredential(string username, UserCredentialDTO updates)
        {
            var credential = await _userManager.FindByNameAsync(username);
            if (credential == null)
                return false;
            var result =
                await _userManager
                    .UpdateAsync(_autoMapper
                        .Map(updates, credential));
            return result.Succeeded;
        }

        public async Task<bool> ChangePassword(string username, string password)
        {
            var credential = await _userManager.FindByNameAsync(username);
            if (credential == null)
                return false;
            credential.PasswordHash = _userManager.PasswordHasher.HashPassword(credential, password);
            var result = await _userManager.UpdateAsync(credential);
            return result.Succeeded;
        }

        public async Task<bool> RemoveCredential(UserCredentialDTO userCredential)
        {
            var credential = await _userManager.FindByNameAsync(userCredential.UserName);
            var result = await _userManager.DeleteAsync(credential);
            return result.Succeeded;
        }

        public async Task<bool> IsCredentialExists(UserCredentialDTO userCredential)
        {
            var record = await _userManager.FindByEmailAsync(userCredential.Email);
            if (record != null)
                return true;
            record = await _userManager.FindByNameAsync(userCredential.UserName);
            return record != null;
        }

        public async Task<string> GenerateToken(UserCredentialDTO userCredential)
        {
            var record = await _userManager.FindByNameAsync(userCredential.UserName);
            var roles = await _userManager.GetRolesAsync(record);
            // ReSharper disable once HeapView.ObjectAllocation.Evident
            // ReSharper disable once HeapView.ObjectAllocation.Evident
            var claims = new List<Claim> {new Claim(ClaimTypes.Name, userCredential.UserName)};
            // ReSharper disable once HeapView.ObjectAllocation
            // ReSharper disable once HeapView.ObjectAllocation.Evident
            claims.AddRange(roles.Select(role => new Claim(ClaimTypes.Role, role)));

            // ReSharper disable once HeapView.ObjectAllocation.Evident
            var key = new SymmetricSecurityKey(
                Encoding.UTF8.GetBytes(_configuration.GetSection("TokenSecurityKey").Value));
            // ReSharper disable once HeapView.ObjectAllocation.Evident
            var credential = new SigningCredentials(key, SecurityAlgorithms.HmacSha512);
            // ReSharper disable once HeapView.ObjectAllocation.Evident
            var tokenDesc = new SecurityTokenDescriptor
            {
                // ReSharper disable once HeapView.ObjectAllocation.Evident
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.Add(new TimeSpan(5, 0, 0, 0, 0)),
                SigningCredentials = credential
            };
            // ReSharper disable once HeapView.ObjectAllocation.Evident
            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDesc);
            return tokenHandler.WriteToken(token);
        }
    }
}