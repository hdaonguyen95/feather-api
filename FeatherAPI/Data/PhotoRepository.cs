﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FeatherAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;

namespace FeatherAPI.Data
{
    public class PhotoRepository : IPhotoRepository
    {
        private readonly IHostEnvironment _host;
        private readonly ICredentialProfileRepository _repository;
        private readonly ApplicationDbContext _context;

        public PhotoRepository(
            IHostEnvironment host,
            ICredentialProfileRepository repository,
            ApplicationDbContext context)
        {
            _host = host;
            _repository = repository;
            _context = context;
        }

        public async Task<Photo> Upload(string username, IFormFile file)
        {
            var credentialProfile = await _repository.RetrieveCredentialProfileByUsername(username);
            if (credentialProfile == null) return null;
            var uploadPath = Path.Combine(_host.ContentRootPath, "uploads");
            if (!Directory.Exists(uploadPath))
                Directory.CreateDirectory(uploadPath);
            var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
            var filePath = Path.Combine(uploadPath, fileName);
            // ReSharper disable once HeapView.ObjectAllocation.Evident
            await using var stream = new FileStream(filePath, FileMode.Create);
            await file.CopyToAsync(stream);
            // ReSharper disable once HeapView.ObjectAllocation.Evident
            var photo = new Photo
            {
                FileName = fileName,
                CredentialProfileIdentity = credentialProfile.Id
            };
            await _context.Photos.AddAsync(photo);
            var saveResult = await _context.SaveChangesAsync() != 0;
            return saveResult ? photo : null;
        }

        public async Task<IEnumerable<Photo>> RetrieveAllPhotosByUsername(string username)
        {
            // ReSharper disable once HeapView.ClosureAllocation
            var credentialProfile = await _repository.RetrieveCredentialProfileByUsername(username);
            if (credentialProfile == null) return null;
            var photos = await _context
                .Photos
                // ReSharper disable once HeapView.ObjectAllocation
                // ReSharper disable once HeapView.ClosureAllocation
                // ReSharper disable once HeapView.ObjectAllocation
                .Where(each => each.CredentialProfileIdentity == credentialProfile.Id)
                .ToListAsync();
            return photos;
        }

        public async Task<Photo> RetrieveSinglePhotosByUsername(string username, string photo)
        {
            // ReSharper disable once HeapView.ClosureAllocation
            var credentialProfile = await _repository.RetrieveCredentialProfileByUsername(username);
            if (credentialProfile == null) return null;
            var result = await _context
                .Photos
                // ReSharper disable once HeapView.ClosureAllocation
                // ReSharper disable once HeapView.ObjectAllocation
                .FirstOrDefaultAsync(each =>
                    each.FileName == photo
                    && each.CredentialProfileIdentity == credentialProfile.Id);
            return result;
        }
    }
}