using System.Collections.Generic;
using System.Threading.Tasks;
using FeatherAPI.Models;

namespace FeatherAPI.Data
{
    public interface ICredentialProfileRepository
    {
        Task<CredentialProfile> CreateCredentialIdentityProfilePair(string credentialIdentity, int profileIdentity);
        Task<CredentialProfile> CreateCredentialIdentityProfilePairWithUsername(string username, int profileIdentity);
        Task<IEnumerable<CredentialProfile>> RetrieveAllCredentialIdentityProfile();
        Task<CredentialProfile> RetrieveCredentialIdentityProfile(int recordNumber);
        Task<CredentialProfile> RetrieveCredentialProfileByProfileIdentity(int profileIdentity);
        Task<CredentialProfile> RetrieveCredentialProfileByCredentialIdentity(string credentialIdentity);
        Task<CredentialProfile> RetrieveCredentialProfileByUsername(string username);
        Task<CredentialProfile> UpdateCredentialProfile(int recordNumber, CredentialProfile credentialProfile);
        Task<CredentialProfile> RemoveCredentialProfile(int recordNumber);
        Task<bool> IsCredentialProfileExists(CredentialProfile credentialProfile);
    }
}