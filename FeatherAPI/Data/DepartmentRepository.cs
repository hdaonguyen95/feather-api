﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FeatherAPI.DTO;
using FeatherAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace FeatherAPI.Data
{
    public class DepartmentRepository : IDepartmentRepository
    {
        private readonly ApplicationDbContext _context;

        public DepartmentRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Department> CreateDepartment(string departmentName)
        {
            var isExists = await IsDepartmentExists(new Department {DepartmentName = departmentName});
            if (isExists)
                return null;

            var department = new Department {DepartmentName = departmentName};
            await _context.Departments.AddAsync(department);

            return await _context.SaveChangesAsync() == 0 ? null : department;
        }

        public async Task<Department> RetrieveDepartmentByNumber(int departmentNumber)
        {
            var record = await _context
                .Departments
                .FirstOrDefaultAsync(result => result.Id == departmentNumber);
            return record;
        }

        public async Task<IEnumerable<Department>> RetrieveAllDepartments()
        {
            var records = await _context.Departments.ToListAsync();
            return records;
        }

        public async Task<Department> UpdateDepartment(int departmentNumber, Department updates)
        {
            var isExists = await IsDepartmentExists(new Department {Id = departmentNumber});
            if (!isExists)
                return null;

            var record = await _context
                .Departments
                .FirstOrDefaultAsync(result => result.Id == departmentNumber);
            record.DepartmentName = updates.DepartmentName;

            return await _context.SaveChangesAsync() == 0 ? null : updates;
        }

        public async Task<bool> RemoveDepartment(int departmentNumber)
        {
            var record = await _context
                .Departments
                .FirstOrDefaultAsync(result => result.Id == departmentNumber);
            if (record == null)
                return false;

            _context.Departments.Remove(record);

            return await _context.SaveChangesAsync() != 0;
        }

        public async Task<bool> IsDepartmentExists(Department department)
        {
            var record = await _context
                .Departments
                .FirstOrDefaultAsync(result => result.Id == department.Id);
            if (record != null)
                return true;
            record = await _context
                .Departments
                .FirstOrDefaultAsync(result => result.DepartmentName == department.DepartmentName);
            return record != null;
        }

        public async Task<Department> GetDepartmentById(int id)
        {
            // ReSharper disable once HeapView.ClosureAllocation
            var record = await _context.Departments
                // ReSharper disable once HeapView.ObjectAllocation
                .FirstOrDefaultAsync(each => each.Id == id);
            return record;
        }

        public async Task<DepartmentReport> GenerateDepartmentReport(int id)
        {
            var records = await _context.Employees
                // ReSharper disable once HeapView.ObjectAllocation
                // ReSharper disable once HeapView.ClosureAllocation
                // ReSharper disable once HeapView.ObjectAllocation
                .Where(each => each.DepartmentForeignKey == id)
                .ToListAsync();
            var all = records.Count();
            // ReSharper disable once HeapView.ClosureAllocation
            var managers = records.Count(employee => employee.IsManager == true);
            // ReSharper disable once HeapView.ClosureAllocation
            var employees = records.Count(employee => employee.IsManager == false);

            // ReSharper disable once HeapView.ObjectAllocation.Evident
            return new DepartmentReport
            {
                All = all,
                Manager = managers,
                Employee = employees
            };
        }
    }
}