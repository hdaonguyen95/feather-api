using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using FeatherAPI.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace FeatherAPI.Data
{
    public class CredentialProfileRepository : ICredentialProfileRepository
    {
        private readonly UserManager<UserCredential> _userManager;
        private readonly ApplicationDbContext _context;
        private readonly IMapper _autoMapper;

        public CredentialProfileRepository(UserManager<UserCredential> userManager, ApplicationDbContext context,
            IMapper autoMapper)
        {
            _userManager = userManager;
            _context = context;
            _autoMapper = autoMapper;
        }

        public async Task<CredentialProfile> CreateCredentialIdentityProfilePair(string credentialIdentity,
            int profileIdentity)
        {
            // ReSharper disable once HeapView.ObjectAllocation.Evident
            var pair = new CredentialProfile
            {
                CredentialIdentity = credentialIdentity,
                ProfileIdentity = profileIdentity
            };
            await _context.AddAsync(pair);
            var result = await _context.SaveChangesAsync() != 0;
            return result ? pair : null;
        }

        public async Task<CredentialProfile> CreateCredentialIdentityProfilePairWithUsername(string username,
            int profileIdentity)
        {
            // ReSharper disable once HeapView.ClosureAllocation
            var credential = await _userManager.FindByNameAsync(username);
            if (credential == null)
                return null;
            
            // ReSharper disable once HeapView.ObjectAllocation.Evident
            var pair = new CredentialProfile
            {
                CredentialIdentity = credential.Id,
                ProfileIdentity = profileIdentity
            };
            await _context.AddAsync(pair);
            var result = await _context.SaveChangesAsync() != 0;
            return result ? pair : null;
        }

        public async Task<IEnumerable<CredentialProfile>> RetrieveAllCredentialIdentityProfile()
        {
            return await _context.CredentialProfiles.ToListAsync();
        }

        public async Task<CredentialProfile> RetrieveCredentialIdentityProfile(int recordNumber)
        {
            // ReSharper disable once HeapView.ClosureAllocation
            // ReSharper disable once HeapView.ObjectAllocation
            return await _context
                .CredentialProfiles
                .FirstOrDefaultAsync(each => each.Id == recordNumber);
        }

        public async Task<CredentialProfile> RetrieveCredentialProfileByProfileIdentity(int profileIdentity)
        {
            var result = await _context
                .CredentialProfiles
                // ReSharper disable once HeapView.ObjectAllocation
                // ReSharper disable once HeapView.ClosureAllocation
                .FirstOrDefaultAsync(each => each.ProfileIdentity == profileIdentity);
            return result;
        }

        public async Task<CredentialProfile> RetrieveCredentialProfileByCredentialIdentity(string credentialIdentity)
        {
            var result = await _context
                .CredentialProfiles
                // ReSharper disable once HeapView.ObjectAllocation
                // ReSharper disable once HeapView.ClosureAllocation
                .FirstOrDefaultAsync(each => each.CredentialIdentity == credentialIdentity);
            return result;
        }

        public async Task<CredentialProfile> RetrieveCredentialProfileByUsername(string username)
        {
            // ReSharper disable once HeapView.ClosureAllocation
            var credential = await _userManager.FindByNameAsync(username);
            if (credential == null)
                return null;

            var credentialProfile =
                await _context
                    .CredentialProfiles
                    // ReSharper disable once HeapView.ClosureAllocation
                    // ReSharper disable once HeapView.ObjectAllocation
                    .FirstOrDefaultAsync(each => each.CredentialIdentity == credential.Id);

            return credentialProfile;
        }

        public async Task<CredentialProfile> UpdateCredentialProfile(int recordNumber,
            CredentialProfile credentialProfile)
        {
            var record = await _context
                .CredentialProfiles
                // ReSharper disable once HeapView.ClosureAllocation
                // ReSharper disable once HeapView.ObjectAllocation
                .FirstOrDefaultAsync(each => each.Id == recordNumber);
            if (record == null)
                return null;

            _autoMapper.Map(credentialProfile, record);
            var result = await _context.SaveChangesAsync() != 0;
            return result ? record : null;
        }

        public async Task<CredentialProfile> RemoveCredentialProfile(int recordNumber)
        {
            var record = await _context
                .CredentialProfiles
                // ReSharper disable once HeapView.ClosureAllocation
                // ReSharper disable once HeapView.ObjectAllocation
                .FirstOrDefaultAsync(each => each.Id == recordNumber);
            if (record == null)
                return null;

            _context.CredentialProfiles.Remove(record);
            var result = await _context.SaveChangesAsync() != 0;
            return result ? record : null;
        }

        public async Task<bool> IsCredentialProfileExists(CredentialProfile credentialProfile)
        {
            var record = await _context
                .CredentialProfiles
                // ReSharper disable once HeapView.ClosureAllocation
                // ReSharper disable once HeapView.ObjectAllocation
                .FirstOrDefaultAsync(each => each.Id == credentialProfile.Id);
            if (record != null)
                return true;

            record = await _context
                .CredentialProfiles
                // ReSharper disable once HeapView.ClosureAllocation
                // ReSharper disable once HeapView.ObjectAllocation
                .FirstOrDefaultAsync(each => each.CredentialIdentity == credentialProfile.CredentialIdentity);
            if (record != null)
                return true;

            record = await _context
                .CredentialProfiles
                // ReSharper disable once HeapView.ClosureAllocation
                // ReSharper disable once HeapView.ObjectAllocation
                .FirstOrDefaultAsync(each => each.ProfileIdentity == credentialProfile.ProfileIdentity);
            return record != null;
        }
    }
}