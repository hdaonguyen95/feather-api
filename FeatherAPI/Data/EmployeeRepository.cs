﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FeatherAPI.DTO;
using FeatherAPI.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace FeatherAPI.Data
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _autoMapper;
        private readonly UserManager<UserCredential> _userManager;
        private readonly ICredentialProfileRepository _credentialProfileRepo;

        public EmployeeRepository(
            ApplicationDbContext context,
            IMapper autoMapper,
            UserManager<UserCredential> userManager,
            ICredentialProfileRepository credentialProfileRepo)
        {
            _context = context;
            _autoMapper = autoMapper;
            _userManager = userManager;
            _credentialProfileRepo = credentialProfileRepo;
        }

        public async Task<Employee> CreateEmployee(Employee employee)
        {
            if (await IsEmployeeExists(employee))
                return null;
            await _context.AddAsync(employee);
            return await _context.SaveChangesAsync() == 0 ? null : employee;
        }

        public async Task<Employee> RetrieveEmployeeByIdentityNumber(int identityNumber)
        {
            return await _context
                .Employees
                // ReSharper disable once HeapView.ClosureAllocation
                // ReSharper disable once HeapView.ObjectAllocation
                .FirstOrDefaultAsync(result => result.Id == identityNumber);
        }

        public async Task<IEnumerable<Employee>> RetrieveAllEmployees()
        {
            return await _context.Employees.ToListAsync();
        }

        public async Task<IEnumerable<EmployeeDTO>> RetrieveAllManager()
        {
            var records = _context
                .Employees
                // ReSharper disable once HeapView.ObjectAllocation
                // ReSharper disable once HeapView.ObjectAllocation
                .Where(employee => employee.IsManager)
                // ReSharper disable once HeapView.ObjectAllocation
                .AsEnumerable()
                // ReSharper disable once HeapView.ObjectAllocation
                // ReSharper disable once HeapView.DelegateAllocation
                .Select(_autoMapper.Map<Employee, EmployeeDTO>)
                .ToList();
            return await Task.FromResult(records);
        }

        public async Task<IEnumerable<EmployeeDTO>> RetrieveEmployeesByDepartment(int departmentNumber)
        {
            var records = _context
                .Employees
                // ReSharper disable once HeapView.ObjectAllocation
                // ReSharper disable once HeapView.ClosureAllocation
                // ReSharper disable once HeapView.ObjectAllocation
                .Where(employee => employee.DepartmentForeignKey == departmentNumber)
                // ReSharper disable once HeapView.ObjectAllocation
                .AsEnumerable()
                // ReSharper disable once HeapView.ObjectAllocation
                // ReSharper disable once HeapView.DelegateAllocation
                .Select(_autoMapper.Map<Employee, EmployeeDTO>)
                .ToList();
            return await Task.FromResult(records);
        }

        public async Task<IEnumerable<EmployeeDTO>> RetrieveEmployeesByManager(int managerIdentityNumber)
        {
            var records = _context
                .Employees
                // ReSharper disable once HeapView.ObjectAllocation
                // ReSharper disable once HeapView.ClosureAllocation
                // ReSharper disable once HeapView.ObjectAllocation
                .Where(employee => employee.ManagerIdentityNumber == managerIdentityNumber)
                // ReSharper disable once HeapView.ObjectAllocation
                .AsEnumerable()
                // ReSharper disable once HeapView.ObjectAllocation
                // ReSharper disable once HeapView.DelegateAllocation
                .Select(_autoMapper.Map<Employee, EmployeeDTO>)
                .ToList();
            return await Task.FromResult(records);
        }

        public async Task<IEnumerable<EmployeeDTO>> RetrieveEmployeesByEmploymentStatus(string status)
        {
            var records = _context
                .Employees
                // ReSharper disable once HeapView.ObjectAllocation
                // ReSharper disable once HeapView.ClosureAllocation
                // ReSharper disable once HeapView.ObjectAllocation
                .Where(employee => employee.EmploymentStatus == status)
                // ReSharper disable once HeapView.ObjectAllocation
                .AsEnumerable()
                // ReSharper disable once HeapView.ObjectAllocation
                // ReSharper disable once HeapView.DelegateAllocation
                .Select(_autoMapper.Map<Employee, EmployeeDTO>)
                .ToList();
            return await Task.FromResult(records);
        }

        public async Task<EmployeeDTO> RetrieveEmployeeByUsername(string username)
        {
            // ReSharper disable once HeapView.ClosureAllocation
            var credentialProfile = await _credentialProfileRepo.RetrieveCredentialProfileByUsername(username);
            if (credentialProfile == null)
                return null;

            var profile =
                // ReSharper disable once HeapView.ClosureAllocation
                // ReSharper disable once HeapView.ObjectAllocation
                await _context
                    .Employees
                    .FirstOrDefaultAsync(each => each.Id == credentialProfile.ProfileIdentity);
            return _autoMapper.Map<Employee, EmployeeDTO>(profile);
        }

        public async Task<EmployeeDTO> RetrieveEmployeeEmailAddress(int identityNumber)
        {
            // ReSharper disable once HeapView.ClosureAllocation
            // ReSharper disable once HeapView.ObjectAllocation
            var record = await _context.Employees.FirstOrDefaultAsync(employee => employee.Id == identityNumber);
            if (record == null)
                return null;
            var credentialProfile = await _credentialProfileRepo
                .RetrieveCredentialProfileByProfileIdentity(record.Id);
            if (credentialProfile == null)
                return null;

            var credential = await _userManager.FindByIdAsync(credentialProfile.CredentialIdentity);
            return credential != null
                // ReSharper disable once HeapView.ObjectAllocation.Evident
                ? new EmployeeDTO
                {
                    EmailAddress = credential.Email
                }
                : null;
        }

        public async Task<Employee> UpdateEmployeeProfile(int identityNumber, Employee updates)
        {
            // ReSharper disable once HeapView.ObjectAllocation.Evident
            var record = await ReturnEmployeeIfExists(new Employee {Id = identityNumber});
            if (record == null)
                return null;
            _autoMapper.Map(updates, record);

            return await _context.SaveChangesAsync() == 0 ? null : updates;
        }

        public async Task<bool> RemoveEmployee(int identityNumber)
        {
            // ReSharper disable once HeapView.ObjectAllocation.Evident
            var record = await ReturnEmployeeIfExists(new Employee {Id = identityNumber});
            if (record == null)
                return false;
            _context.Employees.Remove(record);
            return await _context.SaveChangesAsync() != 0;
        }

        public async Task<bool> IsEmployeeExists(Employee employee)
        {
            var record = await _context
                .Employees
                // ReSharper disable once HeapView.ClosureAllocation
                // ReSharper disable once HeapView.ObjectAllocation
                .FirstOrDefaultAsync(result => result.Id == employee.Id);
            if (record != null)
                return true;
            record = await _context
                .Employees
                // ReSharper disable once HeapView.ClosureAllocation
                // ReSharper disable once HeapView.ObjectAllocation
                .FirstOrDefaultAsync(result => result.SocialSecurityNumber == employee.SocialSecurityNumber);
            return record != null;
        }

        public async Task<Employee> ReturnEmployeeIfExists(Employee employee)
        {
            var record = await _context
                .Employees
                // ReSharper disable once HeapView.ClosureAllocation
                // ReSharper disable once HeapView.ObjectAllocation
                .FirstOrDefaultAsync(result => result.Id == employee.Id);
            if (record != null)
                return record;
            record = await _context
                .Employees
                // ReSharper disable once HeapView.ClosureAllocation
                // ReSharper disable once HeapView.ObjectAllocation
                .FirstOrDefaultAsync(result => result.SocialSecurityNumber == employee.SocialSecurityNumber);
            return record;
        }
    }
}