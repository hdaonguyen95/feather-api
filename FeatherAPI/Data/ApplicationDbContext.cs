﻿using System;
using System.Collections.Generic;
using System.Text;
using FeatherAPI.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FeatherAPI.Data
{
    public class ApplicationDbContext : IdentityDbContext<UserCredential>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Department> Departments { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Photo> Photos { get; set; }
        public DbSet<AuthenticationLog> AuthenticationLogs { get; set; }

        public DbSet<CredentialProfile> CredentialProfiles { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<Employee>()
                .HasOne(entity => entity.Department)
                .WithMany(entity => entity.Employees)
                // ReSharper disable once HeapView.BoxingAllocation
                .HasForeignKey(entity => entity.DepartmentForeignKey);

            builder.Entity<Employee>()
                .HasOne(entity => entity.UserCredential)
                .WithOne(entity => entity.Employee)
                .HasForeignKey<Employee>(entity => entity.CredentialForeignKey);

            builder.Entity<Employee>()
                .HasOne(entity => entity.Manager)
                .WithMany(entity => entity.Employees)
                // ReSharper disable once HeapView.BoxingAllocation
                .HasForeignKey(entity => entity.ManagerIdentityNumber);

            builder.Entity<CredentialProfile>()
                .HasOne(entity => entity.Employee)
                .WithOne(entity => entity.CredentialProfile)
                // ReSharper disable once HeapView.BoxingAllocation
                .HasForeignKey<CredentialProfile>(entity => entity.ProfileIdentity);

            builder.Entity<CredentialProfile>()
                .HasOne(entity => entity.UserCredential)
                .WithOne(entity => entity.CredentialProfile)
                .HasForeignKey<CredentialProfile>(entity => entity.CredentialIdentity);

            builder.Entity<Photo>()
                .HasOne(entity => entity.CredentialProfile)
                .WithMany(entity => entity.Photos)
                // ReSharper disable once HeapView.BoxingAllocation
                .HasForeignKey(entity => entity.CredentialProfileIdentity);

            builder.Entity<AuthenticationLog>()
                .HasOne(entity => entity.UserCredential)
                .WithMany(entity => entity.AuthenticationLogs)
                .HasForeignKey(entity => entity.UserCredentialRef);

            builder.Entity<Department>()
                .HasData(
                    new Department
                    {
                        Id = 1,
                        DepartmentName = "Creative Services"
                    },
                    new Department
                    {
                        Id = 2,
                        DepartmentName = "Product Development"
                    },
                    new Department
                    {
                        Id = 3,
                        DepartmentName = "Marketing"
                    },
                    new Department
                    {
                        Id = 4,
                        DepartmentName = "Strategic Planning"
                    },
                    new Department
                    {
                        Id = 5,
                        DepartmentName = "Accounting"
                    },
                    new Department
                    {
                        Id = 6,
                        DepartmentName = "Human Resources"
                    },
                    new Department
                    {
                        Id = 7,
                        DepartmentName = "Quality Analysis"
                    });
        }
    }
}