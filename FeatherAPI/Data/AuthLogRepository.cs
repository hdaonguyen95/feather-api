using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FeatherAPI.DTO;
using FeatherAPI.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace FeatherAPI.Data
{
    public class AuthLogRepository : IAuthLogRepository
    {
        private readonly UserManager<UserCredential> _manager;
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public AuthLogRepository(UserManager<UserCredential> manager,
            ApplicationDbContext context,
            IMapper mapper)
        {
            _manager = manager;
            _context = context;
            _mapper = mapper;
        }

        public async Task<AuthenticationLog> CreateLog(string username, string ipAddress, string agent)
        {
            var credential = await _manager.FindByNameAsync(username);
            var authLog = new AuthenticationLog
            {
                IpAddress = ipAddress,
                Agent = agent,
                AuthenticationTime = DateTime.Now,
                UserCredentialRef = credential.Id
            };
            await _context.AuthenticationLogs.AddAsync(authLog);
            var result = await _context.SaveChangesAsync() > 0;
            return result ? authLog : null;
        }

        public async Task<IEnumerable<AuthenticationLogDTO>> RetrieveLog(string username)
        {
            var authLogCollection = await _context.AuthenticationLogs.Select(
                    each => _mapper.Map<AuthenticationLog, AuthenticationLogDTO>(each))
                .ToListAsync();
            return authLogCollection;
        }
    }
}