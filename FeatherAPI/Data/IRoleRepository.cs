﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FeatherAPI.Models;

namespace FeatherAPI.Data
{
    public interface IRoleRepository
    {
        Task<bool> AddCredentialToRole(UserCredential credential, string roleName);
        Task<bool> RemoveCredentialFromRole(UserCredential credential, string roleName);
        Task<bool> CreateRole(string roleName);
    }
}