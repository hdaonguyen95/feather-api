﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FeatherAPI.Models;
using Microsoft.AspNetCore.Http;

namespace FeatherAPI.Data
{
    public interface IPhotoRepository
    {
        public Task<Photo> Upload(string username, IFormFile file);
        public Task<IEnumerable<Photo>> RetrieveAllPhotosByUsername(string username);
        public Task<Photo> RetrieveSinglePhotosByUsername(string username, string photo);
    }
}