﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FeatherAPI.DTO;
using FeatherAPI.Models;

namespace FeatherAPI.Data
{
    public interface IEmployeeRepository
    {
        // Create Read Update Delete
        Task<Employee> CreateEmployee(Employee employee);
        Task<Employee> RetrieveEmployeeByIdentityNumber(int identityNumber);
        Task<IEnumerable<Employee>> RetrieveAllEmployees();
        Task<IEnumerable<EmployeeDTO>> RetrieveAllManager();
        Task<IEnumerable<EmployeeDTO>> RetrieveEmployeesByDepartment(int departmentNumber);
        Task<IEnumerable<EmployeeDTO>> RetrieveEmployeesByManager(int managerIdentityNumber);
        Task<IEnumerable<EmployeeDTO>> RetrieveEmployeesByEmploymentStatus(string status);
        Task<EmployeeDTO> RetrieveEmployeeByUsername(string username);
        Task<EmployeeDTO> RetrieveEmployeeEmailAddress(int identityNumber);
        Task<Employee> UpdateEmployeeProfile(int identityNumber, Employee updates);
        Task<bool> RemoveEmployee(int identityNumber);
        Task<bool> IsEmployeeExists(Employee employee);

        Task<Employee> ReturnEmployeeIfExists(Employee employee);
    }
}