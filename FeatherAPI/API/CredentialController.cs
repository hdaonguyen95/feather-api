﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using FeatherAPI.Data;
using FeatherAPI.DTO;
using FeatherAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FeatherAPI.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class CredentialController : ControllerBase
    {
        private readonly ICredentialRepository _repository;
        private readonly IMapper _autoMapper;
        private readonly IHttpContextAccessor _accessor;
        private readonly IAuthLogRepository _authLogRepository;

        public CredentialController(ICredentialRepository repository,
            IMapper autoMapper,
            IHttpContextAccessor accessor,
            IAuthLogRepository authLogRepository)
        {
            _repository = repository;
            _autoMapper = autoMapper;
            _accessor = accessor;
            _authLogRepository = authLogRepository;
        }

        [HttpPost("create")]
        public async Task<IActionResult> CreateCredential(UserCredentialDTO userCredential)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            if (await _repository.IsCredentialExists(userCredential))
            {
                ModelState.AddModelError("BadRequestError", "Credential has been taken");
                return BadRequest(ModelState);
            }

            var result = await _repository.CreateCredential(userCredential);
            if (result != null) return Ok(result);
            ModelState.AddModelError("ServerError",
                "The server can not create your credential at this moment");
            return StatusCode(StatusCodes.Status500InternalServerError, ModelState);
        }
        
        [Authorize]
        [HttpGet("logs/{username}")]
        public async Task<IActionResult> RetrieveAuthenticationLogs(string username)
        {
            var logs = await _authLogRepository.RetrieveLog(username);
            return Ok(logs);
        }

        [HttpPost("authenticate")]
        public async Task<IActionResult> AuthenticateCredential(UserCredentialDTO userCredential)
        {
            var ipAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            var loginAgent = Request.Headers["User-Agent"].ToString();
            var username = userCredential.UserName;

            await _authLogRepository.CreateLog(username, ipAddress, loginAgent);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var result = await _repository.SignIn(userCredential);
            var token = _repository.GenerateToken(userCredential);
            return result ? (IActionResult) Ok(new {token}) : Unauthorized();
        }

        [Authorize]
        [HttpPut("update")]
        public async Task<IActionResult> UpdateCredential(UserCredentialDTO updates)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            if (!await _repository.IsCredentialExists(new UserCredentialDTO {UserName = updates.UserName}))
            {
                ModelState.AddModelError("BadRequestError",
                    "The server can not find any credential that match the provided username");
                return BadRequest(ModelState);
            }

            var result = await _repository.UpdateCredential(updates.UserName, updates);
            if (result) return Ok();
            ModelState.AddModelError("ServerError",
                "The server can not update your credential record at this moment");
            return StatusCode(StatusCodes.Status500InternalServerError, ModelState);
        }

        [Authorize]
        [HttpPost("changePassword")]
        public async Task<IActionResult> ChangePassword(UserCredentialDTO credential)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            if (!await _repository.IsCredentialExists(new UserCredentialDTO
                {UserName = credential.UserName, Email = credential.Email}))
            {
                ModelState.AddModelError("BadRequestError",
                    "The server can not find any credential that match the provided username");
                return BadRequest(ModelState);
            }

            var result = await _repository.ChangePassword(credential.UserName, credential.Password);
            if (result) return Ok();
            ModelState.AddModelError("ServerError",
                "The server can not update your credential password at this moment");
            return StatusCode(StatusCodes.Status500InternalServerError, ModelState);
        }

        [Authorize]
        [HttpDelete("remove")]
        public async Task<IActionResult> RemoveCredential(UserCredentialDTO userCredential)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            if (!await _repository.IsCredentialExists(new UserCredentialDTO {UserName = userCredential.UserName}))
            {
                ModelState.AddModelError("BadRequestError",
                    "The server can not find any credential that match the provided username");
                return BadRequest(ModelState);
            }

            var result = await _repository.RemoveCredential(userCredential);
            if (result) return Ok();
            ModelState.AddModelError("ServerError",
                "The server can not remove your credential record at this moment");
            return StatusCode(StatusCodes.Status500InternalServerError, ModelState);
        }
    }
}