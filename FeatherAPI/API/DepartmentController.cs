﻿using System.Threading.Tasks;
using AutoMapper;
using FeatherAPI.Data;
using FeatherAPI.DTO;
using FeatherAPI.Models;
using FeatherAPI.Roles;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FeatherAPI.API
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentController : ControllerBase
    {
        private readonly IDepartmentRepository _repository;
        private readonly IMapper _autoMapper;

        public DepartmentController(IDepartmentRepository repository, IMapper autoMapper)
        {
            _repository = repository;
            _autoMapper = autoMapper;
        }

        [Authorize(Roles = ApplicationRoles.CanModifyDepartmentDatabase)]
        [HttpPost]
        public async Task<IActionResult> CreateDepartment(DepartmentDTO department)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var result = await _repository.CreateDepartment(department.DepartmentName);

            if (result != null) return Ok(result);
            ModelState.AddModelError("BadRequestError", "Department name has been taken");
            return BadRequest(ModelState);
        }

        [HttpGet("{departmentNumber}")]
        public async Task<IActionResult> RetrieveDepartmentByNumber(int departmentNumber)
        {
            var result = await _repository.RetrieveDepartmentByNumber(departmentNumber);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> RetrieveAllDepartments()
        {
            var result = await _repository.RetrieveAllDepartments();
            return Ok(result);
        }

        [HttpGet("report/{id}")]
        public async Task<IActionResult> GenerateDepartmentReport(int id)
        {
            if (id == 0 || await _repository.GetDepartmentById(id) == null)
            {
                return NotFound();
            }

            var report = await _repository.GenerateDepartmentReport(id);
            return Ok(report);
        }

        [Authorize(Roles = ApplicationRoles.CanModifyDepartmentDatabase)]
        [HttpPut]
        public async Task<IActionResult> UpdateDepartment(DepartmentDTO updates)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            if (!await _repository.IsDepartmentExists(new Department {Id = updates.Id}))
            {
                ModelState.AddModelError("BadRequestError", "The server can not find any department record");
                return BadRequest(ModelState);
            }

            var result = await _repository
                .UpdateDepartment(updates.Id, _autoMapper.Map<DepartmentDTO, Department>(updates));

            if (result != null) return Ok(result);
            ModelState.AddModelError("ServerError", "The server can not update department record at this moment");
            return StatusCode(StatusCodes.Status500InternalServerError, ModelState);
        }

        [Authorize(Roles = ApplicationRoles.CanModifyDepartmentDatabase)]
        [HttpDelete("{departmentNumber}")]
        public async Task<IActionResult> RemoveDepartment(int departmentNumber)
        {
            if (!await _repository.IsDepartmentExists(new Department {Id = departmentNumber}))
            {
                ModelState.AddModelError("BadRequestError", "The server can not find any department record");
                return BadRequest(ModelState);
            }

            var result = await _repository.RemoveDepartment(departmentNumber);
            if (result) return Ok();
            ModelState.AddModelError("ServerError", "The server can not update department record at this moment");
            return StatusCode(StatusCodes.Status500InternalServerError, ModelState);
        }
    }
}