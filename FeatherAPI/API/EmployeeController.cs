﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FeatherAPI.Data;
using FeatherAPI.DTO;
using FeatherAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FeatherAPI.API
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeRepository _repository;
        private readonly IMapper _autoMapper;

        public EmployeeController(IEmployeeRepository repository, IMapper autoMapper)
        {
            _repository = repository;
            _autoMapper = autoMapper;
        }

        [HttpPost]
        public async Task<IActionResult> CreateEmployeeProfile(EmployeeDTO employee)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            if (await _repository.IsEmployeeExists(_autoMapper.Map<EmployeeDTO, Employee>(employee)))
            {
                ModelState.AddModelError("BadRequestError",
                    "The server found duplicate record, please check employee identity number, social security number and try again");
                return BadRequest(ModelState);
            }

            var result = await _repository
                .CreateEmployee(_autoMapper.Map<EmployeeDTO, Employee>(employee));
            if (result != null) return Ok(_autoMapper.Map<Employee, EmployeeDTO>(result));
            ModelState.AddModelError("ServerError", "The server can not create employee profile at this moment");
            return StatusCode(StatusCodes.Status500InternalServerError, ModelState);
        }

        [HttpGet("{identityNumber}")]
        public async Task<IActionResult> RetrieveEmployeeByIdentityNumber(int identityNumber)
        {
            if (!await _repository.IsEmployeeExists(new Employee {Id = identityNumber}))
            {
                ModelState.AddModelError("BadRequestError", "The server can not find any employee record");
                return BadRequest(ModelState);
            }

            var result = await _repository.RetrieveEmployeeByIdentityNumber(identityNumber);
            return Ok(_autoMapper.Map<Employee, EmployeeDTO>(result));
        }

        [HttpGet("username/{username}")]
        public async Task<IActionResult> RetrieveEmployeeByUsername(string username)
        {
            var result = await _repository.RetrieveEmployeeByUsername(username);
            if (result != null) return Ok(result);
            ModelState.AddModelError("BadRequestError", "The server can not find any employee record");
            return BadRequest(ModelState);
        }

        [HttpGet]
        public async Task<IActionResult> RetrieveAllEmployees()
        {
            var result = (await _repository.RetrieveAllEmployees()).ToList();
            // ReSharper disable once HeapView.ObjectAllocation.Evident
            // ReSharper disable once HeapView.ClosureAllocation
            var employees = new List<EmployeeDTO>();
            // ReSharper disable once HeapView.ClosureAllocation
            // ReSharper disable once HeapView.DelegateAllocation
            result.ForEach(employee => { employees.Add(_autoMapper.Map<Employee, EmployeeDTO>(employee)); });
            return Ok(employees);
        }

        [HttpGet("managers")]
        public async Task<IActionResult> RetrieveAllManagers()
        {
            var result = await _repository.RetrieveAllManager();
            return Ok(result);
        }

        [HttpGet("department/{departmentNumber}")]
        public async Task<IActionResult> RetrieveEmployeesByDepartment(int departmentNumber)
        {
            var result = await _repository.RetrieveEmployeesByDepartment(departmentNumber);
            return Ok(result);
        }

        [HttpGet("manager/{managerIdentityNumber}")]
        public async Task<IActionResult> RetrieveEmployeesByManager(int managerIdentityNumber)
        {
            var result = await _repository.RetrieveEmployeesByManager(managerIdentityNumber);
            return Ok(result);
        }

        [HttpGet("status")]
        public async Task<IActionResult> RetrieveEmployeesByEmploymentStatus(EmployeeDTO employee)
        {
            var result = await _repository.RetrieveEmployeesByEmploymentStatus(employee.EmploymentStatus);
            return Ok(result);
        }

        [HttpGet("contact/{identityNumber}")]
        public async Task<IActionResult> RetrieveEmployeeContactEmail(int identityNumber)
        {
            var result = await _repository.RetrieveEmployeeEmailAddress(identityNumber);
            if (result != null) return Ok(result);
            ModelState.AddModelError("BadRequestError",
                "The server can not of any contact email address of the current employee");
            return BadRequest(ModelState);
        }

        [HttpPut("{identityNumber}")]
        public async Task<IActionResult> UpdateEmployee(int identityNumber, EmployeeDTO updates)
        {
            if (!await _repository.IsEmployeeExists(new Employee {Id = identityNumber}))
            {
                ModelState.AddModelError("BadRequestError", "The server can not find any employee record");
                return BadRequest(ModelState);
            }

            var result =
                await _repository
                    .UpdateEmployeeProfile(identityNumber,
                        _autoMapper.Map<EmployeeDTO, Employee>(updates));

            if (result != null) return Ok(_autoMapper.Map<Employee, EmployeeDTO>(result));
            ModelState.AddModelError("ServerError", "The server can not update employee profile at this moment");
            return StatusCode(StatusCodes.Status500InternalServerError, ModelState);
        }

        [HttpDelete("{identityNumber}")]
        public async Task<IActionResult> RemoveEmployeeProfile(int identityNumber)
        {
            if (!await _repository.IsEmployeeExists(new Employee {Id = identityNumber}))
            {
                ModelState.AddModelError("BadRequestError", "The server can not find any employee record");
                return BadRequest(ModelState);
            }

            var result = await _repository.RemoveEmployee(identityNumber);
            if (result) return Ok();
            ModelState.AddModelError("ServerError", "The server can not remove employee profile at this moment");
            return StatusCode(StatusCodes.Status500InternalServerError, ModelState);
        }
    }
}