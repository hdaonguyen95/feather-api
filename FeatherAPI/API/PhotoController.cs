﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using FeatherAPI.Data;
using FeatherAPI.DTO;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;

namespace FeatherAPI.API
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class PhotoController : ControllerBase
    {
        private readonly IPhotoRepository _repository;
        private readonly ICredentialProfileRepository _profileRepository;
        private readonly IHostEnvironment _host;

        public PhotoController(IPhotoRepository repository,
            ICredentialProfileRepository profileRepository,
            IHostEnvironment host)
        {
            _repository = repository;
            _profileRepository = profileRepository;
            _host = host;
        }

        [HttpGet("retrieve/{username}/{photo}")]
        public async Task<IActionResult> RetrievePhotoByPhotoId(string username, string photo)
        {
            if (User.Identity.Name != username)
                return Unauthorized();
            var profileCredentialRecord = await _profileRepository.RetrieveCredentialProfileByUsername(username);
            if (profileCredentialRecord == null)
            {
                ModelState.AddModelError("BadRequestError",
                    "The server can not find any profile record, a profile record is required before uploading any images");
                return BadRequest(ModelState);
            }

            var result = await _repository.RetrieveSinglePhotosByUsername(username, photo);
            if (result == null)
            {
                ModelState.AddModelError("BadRequestError",
                    "The server can not find any photo that match the photo id provided, please check your photo id and try again");
                return BadRequest(ModelState);
            }

            var path = Path.Combine(_host.ContentRootPath, "uploads", result.FileName);
            var binary = await System.IO.File.ReadAllBytesAsync(path);
            return Ok(new PhotoDTO
            {
                Id = result.Id,
                Name = result.FileName,
                PhotoBinary = binary
            });
        }

        [HttpGet("retrieve/{username}")]
        public async Task<IActionResult> RetrieveAllPhotos(string username)
        {
            if (User.Identity.Name != username)
                return Unauthorized();
            var profileCredentialRecord = await _profileRepository.RetrieveCredentialProfileByUsername(username);
            if (profileCredentialRecord == null)
            {
                ModelState.AddModelError("BadRequestError",
                    "The server can not find any profile record, a profile record is required before uploading any images");
                return BadRequest(ModelState);
            }

            // ReSharper disable once HeapView.ObjectAllocation.Evident
            var result = new List<PhotoDTO>();
            var photos = await _repository.RetrieveAllPhotosByUsername(username);
            // ReSharper disable once HeapView.ObjectAllocation.Possible
            foreach (var photo in photos)
            {
                var path = Path.Combine(_host.ContentRootPath, "uploads", photo.FileName);
                var binary = await System.IO.File.ReadAllBytesAsync(path);
                // ReSharper disable once HeapView.ObjectAllocation.Evident
                result.Add(new PhotoDTO
                {
                    Id = photo.Id,
                    Name = photo.FileName,
                    PhotoBinary = binary
                });
            }

            return Ok(result);
        }

        [HttpPost("upload/{username}")]
        public async Task<IActionResult> Upload(string username, IFormFile file)
        {
            if (User.Identity.Name != username)
                return Unauthorized();
            var profileCredentialRecord = await _profileRepository.RetrieveCredentialProfileByUsername(username);
            if (profileCredentialRecord == null)
            {
                ModelState.AddModelError("BadRequestError",
                    "The server can not find any profile record, a profile record is required before uploading any images");
                return BadRequest(ModelState);
            }

            if (file == null)
            {
                ModelState.AddModelError("BadRequestError",
                    "An image file is required for uploading");
                return BadRequest(ModelState);
            }

            if (file.Length == 0)
            {
                ModelState.AddModelError("BadRequestError",
                    "File can not be empty");
                return BadRequest(ModelState);
            }

            if (file.Length > 10 * 1024 * 1024)
            {
                ModelState.AddModelError("BadRequestError",
                    "Image file size can not be larger than 10 megabytes");
                return BadRequest(ModelState);
            }

            var result = await _repository.Upload(username, file);
            if (result != null) return Ok(new {result.FileName});
            ModelState.AddModelError("ServerError",
                "The server can not save new images at this moment, please try again later");
            return StatusCode(StatusCodes.Status500InternalServerError, ModelState);
        }
    }
}