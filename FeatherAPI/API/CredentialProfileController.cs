﻿using System;
using System.Threading.Tasks;
using FeatherAPI.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FeatherAPI.API
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CredentialProfile : ControllerBase
    {
        private readonly ICredentialProfileRepository _repository;

        public CredentialProfile(ICredentialProfileRepository repository)
        {
            _repository = repository;
        }

        [HttpPost("create")]
        public async Task<IActionResult> CreateCredentialProfile(Models.CredentialProfile credentialProfile)
        {
            if (await _repository.IsCredentialProfileExists(credentialProfile))
            {
                ModelState.AddModelError("BadRequestError",
                    "The server found duplicate record");
                return BadRequest(ModelState);
            }

            var result = await _repository
                .CreateCredentialIdentityProfilePair(credentialProfile.CredentialIdentity,
                    credentialProfile.ProfileIdentity);
            return Ok(result);
        }

        [HttpPost("create/{username}")]
        public async Task<IActionResult> CreateCredentialProfileByUsername(string username,
            Models.CredentialProfile credentialProfile)
        {
            // if (await _repository.IsCredentialProfileExists(credentialProfile))
            // {
            //     ModelState.AddModelError("BadRequestError",
            //         "The server found duplicate record");
            //     return BadRequest(ModelState);
            // }

            var result = await _repository
                .CreateCredentialIdentityProfilePairWithUsername(username, credentialProfile.ProfileIdentity);

            return Ok(result);
        }
    }
}