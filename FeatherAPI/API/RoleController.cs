﻿using System.Threading.Tasks;
using FeatherAPI.Data;
using FeatherAPI.DTO;
using FeatherAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FeatherAPI.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly IRoleRepository _repository;

        public RoleController(IRoleRepository repository)
        {
            _repository = repository;
        }

        [HttpPost("createRole")]
        public async Task<IActionResult> CreateRole(RoleDTO role)
        {
            var result = await _repository.CreateRole(role.RoleName);
            if (result) return Ok();
            ModelState.AddModelError("ServerError", "The server can not create new role at this moment");
            return StatusCode(StatusCodes.Status500InternalServerError, ModelState);
        }

        [HttpPost("add")]
        public async Task<IActionResult> AddToRole(RoleDTO role)
        {
            var result =
                await _repository.AddCredentialToRole(new UserCredential {UserName = role.UserName}, role.RoleName);
            if (result) return Ok();
            ModelState.AddModelError("ServerError", "The server can not add credential to role at this moment");
            return StatusCode(StatusCodes.Status500InternalServerError, ModelState);
        }

        [HttpPost("remove")]
        public async Task<IActionResult> RemoveFromRole(RoleDTO role)
        {
            var result =
                await _repository.RemoveCredentialFromRole(new UserCredential {UserName = role.UserName},
                    role.RoleName);
            if (result) return Ok();
            ModelState.AddModelError("ServerError", "The server can not remove credential from role at this moment");
            return StatusCode(StatusCodes.Status500InternalServerError, ModelState);
        }
    }
}