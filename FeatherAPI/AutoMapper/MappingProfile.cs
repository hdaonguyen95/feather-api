﻿using AutoMapper;
using FeatherAPI.DTO;
using FeatherAPI.Models;

namespace FeatherAPI.AutoMapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<DepartmentDTO, Department>();

            CreateMap<Employee, Employee>()
                .ForMember(employee => employee.Id,
                    opt => opt.Ignore());
            CreateMap<EmployeeDTO, Employee>()
                .ForMember(employee => employee.Id,
                    opt => opt.Ignore());
            CreateMap<Employee, EmployeeDTO>();
            CreateMap<UserCredentialDTO, UserCredential>();
            CreateMap<CredentialProfile, CredentialProfile>()
                .ForMember(entity => entity.Id, option => option.Ignore());
            CreateMap<AuthenticationLog, AuthenticationLogDTO>();
        }
    }
}