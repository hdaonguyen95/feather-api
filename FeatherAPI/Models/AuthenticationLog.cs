using System;

namespace FeatherAPI.Models
{
    public class AuthenticationLog
    {
        public int Id { get; set; }
        public string IpAddress { get; set; }
        public string Agent { get; set; }
        public DateTime AuthenticationTime { get; set; }
        public string UserCredentialRef { get; set; }
        public UserCredential UserCredential { get; set; }
    }
}