﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FeatherAPI.Models
{
    public class Department
    {
        public int Id { get; set; }
        public string DepartmentName { get; set; }
        public IEnumerable<Employee> Employees { get; set; }
    }
}