﻿using System.ComponentModel.DataAnnotations;

namespace FeatherAPI.Models
{
    public class Photo
    {
        public int Id { get; set; }
        [Required] [StringLength(255)] public string FileName { get; set; }
        public int CredentialProfileIdentity { get; set; }
        public CredentialProfile CredentialProfile { get; set; }
    }
}