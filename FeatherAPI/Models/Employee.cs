﻿using System;
using System.Collections.Generic;

namespace FeatherAPI.Models
{
    public class Employee
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string SocialSecurityNumber { get; set; }
        public string AddressStreet { get; set; }
        public string AddressCity { get; set; }
        public string AddressState { get; set; }
        public string AddressPostalCode { get; set; }
        public bool IsManager { get; set; }
        public string EmploymentStatus { get; set; }
        public DateTime HiredDate { get; set; }

        public int? ManagerIdentityNumber { get; set; }
        public Employee Manager { get; set; }
        public IEnumerable<Employee> Employees { get; set; }

        public int? DepartmentForeignKey { get; set; }
        public Department Department { get; set; }

        public string CredentialForeignKey { get; set; }
        public UserCredential UserCredential { get; set; }

        public CredentialProfile CredentialProfile { get; set; }
    }
}