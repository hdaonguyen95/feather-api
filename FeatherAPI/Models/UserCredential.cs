﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace FeatherAPI.Models
{
    public class UserCredential : IdentityUser
    {
        public Employee Employee { get; set; }
        public CredentialProfile CredentialProfile { get; set; }
        public IEnumerable<AuthenticationLog> AuthenticationLogs { get; set; }
    }
}