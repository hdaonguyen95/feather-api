using System.Collections.Generic;

namespace FeatherAPI.Models
{
    public class CredentialProfile
    {
        public int Id { get; set; }
        public string CredentialIdentity { get; set; }
        public UserCredential UserCredential { get; set; }
        public int ProfileIdentity { get; set; }
        public Employee Employee { get; set; }

        public IEnumerable<Photo> Photos { get; set; }
    }
}